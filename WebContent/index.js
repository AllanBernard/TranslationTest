function load() {
	var translate = new Translate();
	var currentLng = getParameterByName("lang");
	if (currentLng == null)
		currentLng = "eng";
	var attributeName = 'data-tag';
	translate.init(attributeName, currentLng);
	translate.process();
}

function changeLanguage(){
	if(getParameterByName("lang")=="est"){
		setParameterByName("lang", "eng");
	}else{
		setParameterByName("lang", "est");
	}
	load();
}

function Translate() {
	this.init = function(attribute, lng) {
		this.attribute = attribute;
		this.lng = lng;
	}
	this.process = function() {
		_self = this;
		var xrhFile = new XMLHttpRequest();
		xrhFile.open("GET", "./" + this.lng + ".json", false);
		xrhFile.onreadystatechange = function() {
			if (xrhFile.readyState === 4) {
				if (xrhFile.status === 200 || xrhFile.status == 0) {
					var LngObject = JSON.parse(xrhFile.responseText);
					var allDom = document.getElementsByTagName("*");
					for (var i = 0; i < allDom.length; i++) {
						var elem = allDom[i];
						var key = elem.getAttribute(_self.attribute);
						if (key != null) {
							elem.innerHTML = LngObject[key];
						}
					}
				}
			}
		}
		xrhFile.send();
	}
}

function getParameterByName(name, url) {
	if (!url)
		url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex
			.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function setParameterByName(name, value, url) {
	if (!url)
		url = window.location;
	name = escape(name);
	value = escape(value);
	var nvp = url.search.substr(1).split('&');
	if (nvp == '') {
		url.search = '?' + name + '=' + value;
	} else {
		var i = nvp.length;
		var x;
		while (i--) {
			x = nvp[i].split('=');
			if (x[0] == name) {
				x[1] = value;
				nvp[i] = x.join('=');
				break;
			}
		}
		if (i < 0) {
			nvp[nvp.length] = [ name, value ].join('=');
		}
		url.search = nvp.join('&');
	}
}